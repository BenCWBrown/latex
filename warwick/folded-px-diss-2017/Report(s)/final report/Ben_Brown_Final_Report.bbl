\begin{thebibliography}{10}

\bibitem{hitchin_2015}
N.~{Hitchin}, ``\href{https://arxiv.org/abs/1501.04989}{Higgs bundles and
  diffeomorphism groups},'' {\em ArXiv e-prints}, Jan. 2015.

\bibitem{biquard_2015}
O.~{Biquard}, ``{\href{https://arxiv.org/abs/1503.04128}{M{\'e}triques
  hyperk{\"a}hleriennes pli{\'e}es}},'' {\em ArXiv e-prints}, Mar. 2015.

\bibitem{dasilva_2000}
G.~V. {da Silva}, C. and C.~{Woodward}, ``{On the Unfolding of Folded
  Symplectic Structures},'' {\em Math. Res. Lett.}, vol.~7, p.~35, 2000.

\bibitem{baykur_2006}
R.~{Baykur}, ``{\href{https://arxiv.org/abs/math/0601396}{Kahler Decomposition
  of 4-Manifolds}},'' {\em ArXiv e-prints}, Jan. 2006.

\bibitem{ashtekar_1988}
J.~T. {Ashtekar}, A. and L.~{Smolin}, ``{A New Characterization of Half-Flat
  Solutions to Einstein's Equation},'' {\em Comm. Math. Phys.}, vol.~115,
  no.~4, p.~631, 1988.

\bibitem{gibbons_2013}
G.~W. {Gibbons} and N.~P. {Warner},
  ``{\href{https://arxiv.org/abs/1305.0957}{Global Structure of
  Five-Dimensional Fuzzballs}},'' {\em Class. Quant. Grav.}, vol.~31, jan 2014.

\bibitem{hitchin_1991}
N.~{Hitchin}, ``{Hyperk\"ahler manifolds},'' {\em S\'eminaire Bourbaki},
  vol.~34, p.~137, 1991.

\bibitem{berger_1955}
M.~{Berger}, ``{Sur les Groupes d'Holonomie Homog\`enes de Vari\'et\'es \`a
  Connexion Affine et des Vari\'et\'es Riemanniennes},'' {\em {Bulletin de la
  Soci\'et\'e Math\'ematique de France}}, vol.~83, p.~279, 1955.

\bibitem{dancer_1994}
A.~S. {Dancer}, ``{A Family of Hyperk\"ahler Manifolds},'' {\em The Quarterly
  Journal of Mathematics}, vol.~45, no.~4, p.~463, 1994.

\bibitem{eguchi_1978}
T.~{Eguchi} and A.~J. {Hanson}, ``{Asymptotically Flat Self-Dual Solutions to
  Euclidean Gravity},'' {\em Phys. Lett. B}, vol.~74, no.~3, p.~249, 1978.

\bibitem{gibbons_1978}
G.~W. {Gibbons} and S.~W. {Hawking}, ``{Gravitational Multi-Instantons},'' {\em
  Phys. Lett. B}, vol.~78, no.~4, p.~430, 1978.

\bibitem{hitchin_1987}
K.~A. L.~U. {Hitchin}, N.~J. and M.~{Ro\v{c}ek}, ``{Hyper-K\"ahler Metrics and
  Supersymmetry},'' {\em Comm. Math. Phys.}, vol.~108, no.~4, p.~535, 1987.

\bibitem{eguchi_1980}
T.~{Eguchi}, P.~B. {Gilkey}, and A.~J. {Hanson}, ``{Gravitation, Gauge Theories
  and Differential Geometry},'' {\em Phys. Rep.}, vol.~66, p.~213, dec 1980.

\bibitem{capovilla_1993}
R.~{Capovilla} and J.~{Pleba\'nski}, ``{Some Exact Solutions of the Einstein
  Field Equations in Terms of the Self‐Dual Spin Connection},'' {\em {Jour.
  of Math. Phys.}}, vol.~34, no.~1, p.~130, 1993.

\bibitem{capovilla_1989}
J.~T. {Capovilla}, R. and J.~{Dell}, ``{General Relativity without the
  Metric},'' {\em Phys. Rev. Lett.}, vol.~63, p.~2325, Nov 1989.

\bibitem{cahen_1967}
D.~R. {Cahen}, M. and L.~{Defrise}, ``{A Complex Vectorial Formalism in General
  Relativity},'' {\em {Jour. Math. Mech.}}, vol.~16, no.~7, p.~761, 1967.

\bibitem{plebanski_1975}
J.~F. {Pleba\'nski}, ``{Some Solutions of Complex Einstein Equations},'' {\em
  Jour. Math. Phys.}, vol.~16, no.~12, 1975.

\bibitem{hooft_1976}
G.~{'t Hooft}, ``{Computation of the Quantum Effects due to a Four-Dimensional
  Pseudoparticle},'' {\em Phys. Rev. D}, vol.~14, no.~12, p.~3432, 1976.

\bibitem{capovilla_1991}
R.~{Capovilla}, J.~{Dell}, T.~{Jacobson}, and L.~{Mason}, ``{Self-Dual 2-forms
  and Gravity},'' {\em Class. Quant. Grav.}, vol.~8, no.~1, p.~41, 1991.

\bibitem{solitons}
M.~{Dunajski}, {\em Solitons, Instantons and Twistors}.
\newblock Oxford Graduate Texts in Mathematics, 2010.

\bibitem{hashimoto_1997}
Y.~{Hashimoto}, Y.~{Yasui}, S.~{Miyagi}, and T.~{Ootsuka},
  ``{\href{https://arxiv.org/abs/hep-th/9610069}{Applications of the Ashtekar
  Gravity to Four Dimensional Hyperk{\"a}hler Geometry and Yang-Mills
  Instantons}},'' {\em Journ. Math. Phys.}, vol.~38, p.~5833, 1997.

\bibitem{donaldson}
S.~K. {Donaldson}, ``{Complex Cobordism, Ashtekar's Equations and
  Diffeomorphisms},'' in {\em {Symplectic Topology}} (D.~{Salamon}, ed.),
  London Math. Soc., 1992.

\bibitem{ootsuka_1998}
T.~{Ootsuka}, S.~{Miyagi}, Y.~{Yasui}, and S.~{Zeze},
  ``{\href{https://arxiv.org/abs/gr-qc/9809083v1}{Anti-Self-Dual Maxwell
  Solutions on Hyper-K{\"a}hler Manifold and N=2 Supersymmetric Ashtekar
  gravity}},'' {\em Class. Quant. Grav.}, vol.~16, p.~1305, Apr. 1999.

\bibitem{joyce_1995}
D.~{Joyce}, ``{Explicit Construction of Self-Dual 4-Manifolds},'' {\em Duke
  Math. Jour.}, vol.~77, p.~519, 1995.

\bibitem{grant_1997}
J.~D.~E. {Grant}, ``{\href{https://arxiv.org/abs/gr-qc/9702001v1}{Self-Dual
  Two-Forms and Divergence-Free Vector Fields}},'' 1997.

\bibitem{tod_1995}
K.~P. {Tod}, ``{Scalar-Flat K\"ahler and HyperK\"ahler Metrics from
  Painlev\'e-III},'' {\em {Class. Quant. Grav.}}, vol.~12, no.~6, p.~1535,
  1995.

\bibitem{lebrun_1991}
C.~{LeBrun}, ``{Explicit Self-Dual Metrics on $\mathbb{CP}_2 \#
  \cdots\#\mathbb{CP}_2$},'' {\em Jour. Diff. Geom.}, vol.~34, no.~1, p.~223,
  1991.

\bibitem{park_1990}
Q.~{Park}, ``{Extended Conformal Symmetries in Real Heavens},'' {\em {Phys.
  Lett. B}}, vol.~236, no.~4, p.~429, 1990.

\bibitem{bakas_1995}
I.~{Bakas} and K.~{Sfetsos}, ``{T-Duality and World-Sheet Supersymmetry},''
  {\em {Phys. Lett. B}}, vol.~349, p.~448, Feb. 1995.

\bibitem{ashtekar_1987}
A.~{Ashtekar}, ``{New Hamiltonian Formulation of General Relativity},'' {\em
  Phys. Rev. D}, vol.~36, no.~6, 1987.

\bibitem{mason_1989}
L.~J. {Mason} and E.~T. {Newman}, ``{A Connection between the Einstein and
  Yang-Mills Equations},'' {\em Comm. Math. Phys.}, vol.~121, no.~4, p.~659,
  1989.

\bibitem{bryant_2004}
R.~L. {Bryant}, ``{Real Hypersurfaces in Unimodular Complex Surfaces},'' {\em
  ArXiv Mathematics e-prints}, July 2004.

\end{thebibliography}
