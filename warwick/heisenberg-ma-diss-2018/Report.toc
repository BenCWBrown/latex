\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Elliptic Curves over $\mathbb {C}$}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}The Theorem of Riemann-Roch}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Elliptic Curves as Complex Tori}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Complex Tori as Elliptic Curves}{6}{subsection.2.3}
\contentsline {section}{\numberline {3}Theta Functions}{9}{section.3}
\contentsline {section}{\numberline {4}Projectively Embedded Elliptic Curves}{15}{section.4}
\contentsline {subsection}{\numberline {4.1}The Hesse Cubic}{16}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}The General Case for $n > 3$}{18}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}The Fermat Quartic}{22}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}The Bianchi Quintic}{23}{subsection.4.4}
\contentsline {section}{\numberline {5}The Heisenberg Group and Abstract Configurations}{24}{section.5}
\contentsline {subsection}{\numberline {5.1}The Heisenberg Group and Schr{\"o}dinger Representation}{24}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Abstract Configurations}{25}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}The Normaliser of the Heisenberg Group}{29}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Examples}{30}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}The Hesse Pencil}{30}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}The Bianchi Quintic}{34}{subsubsection.5.4.2}
\contentsline {section}{\numberline {6}Conclusion}{36}{section.6}
